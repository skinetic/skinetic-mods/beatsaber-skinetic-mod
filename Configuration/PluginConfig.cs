﻿using IPA.Config.Stores;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo(GeneratedStore.AssemblyVisibilityTarget)]
namespace Skinetic_BeatSaber_Mod.Configuration
{
    internal class PluginConfig
    {
        public static PluginConfig Instance { get; set; }
        //public virtual int IntValue { get; set; } = 42; // Must be 'virtual' if you want BSIPA to detect a value change and save the config automatically.


        public virtual float VolumePercentage { get; set; } = 100.0f;
        public virtual uint SerialNumber { get; set; } = 0;
        public virtual int BoostPercent { get; set; } = 0;
        public virtual float MusicEffectsVolume { get; set; } = 80.0f;
        public virtual bool MusicEffectsEnabled { get; set; } = true;

        /// <summary>
        /// This is called whenever BSIPA reads the config from disk (including when file changes are detected).
        /// </summary>
        public virtual void OnReload()
        {
            // Do stuff after config is read from disk.
        }

        /// <summary>
        /// Call this to force BSIPA to update the config file. This is also called by BSIPA if it detects the file was modified.
        /// </summary>
        public virtual void Changed()
        {
            // Do stuff when the config is changed.
        }

        /// <summary>
        /// Call this to have BSIPA copy the values from <paramref name="other"/> into this config.
        /// </summary>
        public virtual void CopyFrom(PluginConfig other)
        {
            // This instance's members populated from other
        }
    }
}
