# Beat Saber - Skinetic Mod

<p align="center">

[![Skinetic Discord](https://dcbadge.vercel.app/api/server/55u4DsCP82)](https://discord.gg/55u4DsCP82)

</p>


**Support for Skinetic haptic vest on Beat Saber interactions.**

> ⚠ This mod uses Skinetic SDK v1.6.4 and must be used with Skinetic Vest version 1.3.0 or later

## Description

It provides distinct effects for these in-game interactions:
- Perfect Note Slicing (left/right)
- Wrong Note Slicing (left/right)
- Bomb exploding (left/right)
- Missed Note (left/right)
- Multiplier increase
- Arc (left/right)
- Wall collision
- Level cleared
- Level failed
- UI hovering
- UI click
- Experimental Ring rotation effects
- Experimental Light event effects

---

## Table of Contents

- [Usage](#usage)
    - [Requirements](#requirements)
    - [Install](#install)
    - [Launch](#launch)
    - [Settings](#settings)
    - [Edit Haptic Patterns](#edit-haptic-patterns)
- [Mod Edition](#mod-edition)
    - [Project](#project)
    - [Code Structure](#code-structure)
- [Version History](#version-history)
- [License](#license)
- [Help](#help)

----

## Usage

### Requirements

- **Skinetic** Vest
- [**Beat Saber**](https://beatsaber.com/#start-playing) for PCVR
- _(optional)_ [**ModAssistant**](https://github.com/Assistant/ModAssistant)
- [**BSIPA**](https://nike4613.github.io/BeatSaber-IPA-Reloaded/) (can also be installed by _ModAssistant_)

### Install

- Install BSIPA (from ModAssistant or standalone)
- Extract the archive to the root folder of your game
    - If you use Steam with default game location, it should be in `C:\Program Files\Steam\steamapps\common\Beat Saber`
    - It should put `SkineticSDK.dll` in the _Libs_ folder, and `Skinetic_BeatSaber_Mod.dll` in _Plugins_

### Launch

- Turn on the Skinetic vest
- Connect it with usb or turn on Bluetooth (pair it to your computer if needed)
- Start the game as usual

> The vest will indicate its connection with a small haptic feedback.

### Settings

On first start, the mod should create a `Skinetic_BeatSaber_Mod.json` file inside the _UserData_ directory.

The `Skinetic_BeatSaber_Mod.json` file can be edited with any text editor. It contains 3 editable values:

- **DefaultVolume**: Controls the global volume of the patterns (in %).
     The value can be set from 0 to 250. At 100, the patterns base volume is preserved and rendered as intended. Between 0 and 99, it's attenuated. Between 101 and 250, the volume is increased, which can lead to distortion of the effects and product audible noise.

- **SerialNumber**: Default value is 0. This is the serial number of the Skinetic vest the mod will try to connect to. 0 means the first available Skinetic vest. 
This setting should only be changed if multiple vests are paired to the PC being used.

- **BoostPercent**: int. From 0 to 100. Default value is 0%.
    This setting allows the end user to globally boost the effects played on the vest. At 0%, no boost is applied. From 1 to 100%, each pattern is **altered** to create a more punchy feel on Skinetic. For this mod, keeping this value below 40% will preserve most of the intended feel of each effect.

- **MusicEffectsVolume**: Volume of the experimental effects (light and spin effects). The value can be set from 0 to 250. At 100, the patterns base volume is preserved and rendered as intended. Between 0 and 99, it's attenuated. Between 101 and 250, the volume is increased, which can lead to distortion of the effects and product audible noise.

- **MusicEffectsEnabled**: Enable or disable experimental effects. Those effects may introduce lags, disabling them will reduce such lags.

### Edit Haptic Patterns

At first start, the mod should also create a new folder `SkineticPatterns` with the different haptic patterns inside the _UserData_ directory.

Patterns located in `UserData/SkineticPatterns/` can be modified using [**Unitouch Studio**](https://www.skinetic.actronika.com/unitouch-studio).
Each `[patternName].spn` file can be directly loaded and modified in the studio or completely replaced by a new file.
To be triggered correctly, the new file must have the same name as the one it replaces.

To reset the patterns, simply delete or rename the folder containing them. This will force the mod to regenerate them the next times it starts.

---

## Mod edition

For more advanced users and mod developprs, this mod can be easily edited and updated.

### Project

- This project is a Visual Code project based on the BSIPA template. (See [BSIPA PC Mod Development Documentation](https://bsmg.wiki/modding/pc-mod-dev-intro.html)). This template simplifies the task by allowing you to select the location of the game files and automatically build and move the mod to the right place for testing.

### Code structure

The _SkineticSDK library_ is located in the `libs` folder.

Haptic patterns file are in the `SkineticPatterns` directory and should only be replaced or updated but never removed or renamed for the mod to work properly as is. 

Each pattern is loaded as a text Resource within the Visual Studio project.
Additional patterns must also be added to be automatically generated by the mod on first start. _(See Project Properties > Resources menu)._


Interesting code files are separated into 3 folders:

- In the root folder:
    - `Skinetic_BeatSaber_ModController.cs`: This is the main class of the mod. It contains all mod patches and loads them into the game using _Harmony_. Each patch calls Skinetic Manager methods to communicate with the vest.

- In the `Skinetic` folder:
    - `SkineticWrapping.cs`: This is the wrapper for the SkineticSDK dll. It handles basic conversion from C# to C++. It also contains documentation for each of the available methods. **Should not be modified**.
    - `SkineticManager.cs`: _Skinetic Manager_ is a class designed to simplify the use of the SDK itself. It handles basic tasks such as initialization, connection/disconnection, pattern loading, automatic pattern folder generation on startup and also simplified play/stop methods.
    **This class can be adapted to better suit your needs.**

- In the `Configuration` folder:
    - `PluginConfig.cs`: This class contains all settings that are editable in the json itself. Values defined here are default values. Each variable can be accessed by other classes using `PluginConfig.Instance.[VarName]`


## Version History

* 2.0.0
    * Rework of existing effects with vicinities
    * SDK upgrade --> Switch to streaming mode
    * Added Multiplier, Arc, UI and environemental effects
    * Automatic reco
* 1.0.0
    * Initial release


## License

- This mod is licensed under LGPL-v3.
- It includes Skinetic SDK as a closed-source library. See [Skinetic SDK CGL](./ACTRONIKA%20-%20CGL%20Skinetic%20SDK%20-%20EN.pdf).

## Help

To get help, discuss good practices or report any issue, join the [Skinetic discord server](https://discord.gg/55u4DsCP82).