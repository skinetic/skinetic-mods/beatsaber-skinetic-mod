﻿/*
 * This file is part of Arizona Sunshine Skinetic Mod.
 *
 * Arizona Sunshine Skinetic Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Skinetic Mod Template is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Arizona Sunshine Skinetic Mod.  If not, see <http://www.gnu.org/licenses/>.
 */

using Skinetic_BeatSaber_Mod;
using Skinetic_BeatSaber_Mod.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Threading;
using static Skinetic.SkineticSDK;

namespace Skinetic
{
    [Flags] // FlagsAttribute enum allows us to combine multiple values of Pattern and ensurw the result is not already used.
    public enum Patterns : Int32 // Int32 type can be changed for Int64 if the limit of 32 patterns is reached
    {
        None = 0, // default - not used

        PerfectNote = 1 << 0,
        ImperfectNote = 1 << 1,
        CutVertical = 1 << 2,
        CutHorizontal = 1 << 3,
        CutDiagonal = 1 << 4,

        Bomb = 1 << 5,
        Missed = 1 << 6,
        Wall = 1 << 7,

        LevelCleared = 1 << 8,
        LevelFailed = 1 << 9,

        UIHover = 1 << 10,
        UIClick = 1 << 11,

        Connect = 1 << 12,

        X = 1 << 13,
        Stay = 1 << 14,
        Hit = 1 << 15,

        Kick = 1 << 16,
        Spin = 1 << 17,
        LightBoost = 1 << 18,

        H1 = 1 << 21,
        H2 = 1 << 22,
        H3 = 1 << 23,
        H4 = 1 << 24,
        H5 = 1 << 25,
        H6 = 1 << 26,
        H7 = 1 << 27,
        H8 = 1 << 28,

        Arc = 1 << 29,

        // Add new value to link new effect here
        // numeral value should be the next power of two (eg. (1 << 16))
    };

    /// <summary>
    /// Class <c>SkineticManager</c> handles connection/disconnection of the devices, 
    /// automatic patterns loading and basic calls to ease the use from the mod.
    /// </summary>
    class SkineticManager
    {
        private SkineticSDK _skineticInstance;
        private Dictionary<String, int> _patternDict = new Dictionary<string, int>();
        private Dictionary<Patterns, int> _patternsIds = new Dictionary<Patterns, int>();
        private string _skineticPatternsFolder = "SkineticPatterns";
        public bool _slowDetectStatus = false;

        public ConnectionState vestStatus = ConnectionState.E_DISCONNECTED;

        /// <summary>
        /// <c>SkineticManager</c> constructor inits the Skinetic SDK instance, 
        /// connects to the device according to the parameter entered in the config file 
        /// (0 for the first available, a valid serial number for a specific Skinetic vest),
        /// and loads the pattern available on disk.
        /// </summary>
        public SkineticManager()
        {
            _skineticInstance = new SkineticSDK();
            _skineticInstance.InitInstance();
            _skineticInstance.Exp_EnableLegacyBackend(false);

            _skineticInstance.Connect(SkineticSDK.OutputType.E_AUTODETECT, PluginConfig.Instance.SerialNumber);
            Plugin.Log?.Debug("Try to connect to first Skinetic device available");

            _skineticInstance.SetConnectionCallback(ConnectionCallback);

            Thread connectionThread = new Thread(() => ConnectionStatus(this, _skineticInstance));
            connectionThread.Start();

            LoadPatterns();
        }

        /// <summary>
        /// <c>Disconnect</c> stops all effect playing and handles the device disconnection and sdk deinstantiation.
        /// </summary>
        public void Disconnect()
        {
            _skineticInstance.StopAll();
            _skineticInstance.Disconnect();
            _skineticInstance.DeinitInstance();
        }

        public string GetSerialNumber()
        {
            string sN = _skineticInstance.GetDeviceSerialNumberAsString();
            return sN;
        }

        /// <summary>
        /// <c>Play</c> method takes a patterns name, check its existence among the loaded patterns
        /// and plays it if existing with the given parameters. Volume is modulated by the Volume set in the config file.
        /// </summary>
        /// <param name="pattern">Patterns.value of the pattern to play</param>
        /// <param name="properties">struct to specialized the effect.</param>
        /// <returns>Id of the effect played or -1 if the pattern is not found</returns>
        public int Play(Patterns pattern, SkineticSDK.EffectProperties properties)
        {
            properties.volume = properties.volume * (PluginConfig.Instance.VolumePercentage / 100f);
            if(PluginConfig.Instance.BoostPercent != 0)
            {
                properties.overridePatternBoost = true;
                properties.effectBoost = PluginConfig.Instance.BoostPercent;
            }
            if (_patternsIds.ContainsKey(pattern))
            {
                return _skineticInstance.PlayEffect(_patternsIds[pattern], properties);
            }
            Plugin.Log?.Debug("Pattern not found: " + pattern);
            return -1;
        }

        /// <summary>
        /// <c>PlayWithTransform</c> method takes a patterns name, check its existence among the loaded patterns
        /// and plays it if existing with the given parameters. It only takes priority and transform parameters. For more adjustments, use the <c>Play</c> method.
        /// </summary>
        /// <param name="pattern">Patterns.value of the pattern to play</param>
        /// <param name="priority">Level of priority [1; 10] (default - 5). In case too
        /// many effects are playing simultaneously, the effect with lowest
        /// priority(10) will be muted.</param>
        /// <param name="height">Normalized height [-1; 1] to translate the pattern by (default - 0). A positive value translate
        /// the pattern upwards.</param>
        /// <param name="heading">Heading angle (in degree) to rotate the pattern by in the horizontal plan (default - 0). A positive
        /// value rotates the pattern to the left of the vest.</param>
        /// <param name="tilting">Tilting angle (in degree) to rotate the pattern by in the sagittal plan (default - 0). A positive
        /// value rotates the pattern upwards from front to back.</param>
        /// <param name="frontBackInversion">Invert the direction of the pattern on the front-back axis (default - false). Can be combine with other
        /// inversion or addition.</param>
        /// <param name="upDownInversion">Invert the direction of the pattern on the up-down axis (default - false). Can be combine with other
        /// inversion or addition.</param>
        /// <param name="rightLeftInversion">Invert the direction of the pattern on the right-left axis (default - false). Can be combine with other
        /// inversion or addition.</param>
        /// <param name="frontBackAddition">Perform a front-back addition of the pattern on the front-back axis (default - false). Overrides the
        /// frontBackInversion.Can be combine with other inversion or addition.</param>
        /// <param name="upDownAddition">Perform a up-down addition of the pattern on the front-back axis (default - false). Overrides the
        /// upDownInversion.Can be combine with other inversion or addition.</param>
        /// <param name="rightLeftAddition">Perform a right-left addition of the pattern on the front-back axis (default - false). Overrides the
        /// rightLeftInversion.Can be combine with other inversion or addition.</param>
        /// <returns>Id of the effect played or -1 if the pattern is not found</returns>
        public int PlayWithTransform(Patterns pattern, int priority = 5,
                                    float height = 0, float heading = 0, float tilting = 0,
                                    bool frontBackInversion = false, bool upDownInversion = false, bool rightLeftInversion = false,
                                    bool frontBackAddition = false, bool upDownAddition = false, bool rightLeftAddition = false)
        {
            var prop = InitEffectProperties();
            prop.priority = priority;
            prop.height = height;
            prop.heading = heading;
            prop.tilting = tilting;
            prop.frontBackInversion = frontBackInversion;
            prop.upDownInversion = upDownInversion;
            prop.rightLeftInversion = rightLeftInversion;
            prop.frontBackAddition = frontBackAddition;
            prop.upDownAddition = upDownAddition;
            prop.rightLeftAddition = rightLeftAddition;
            return Play(pattern, prop);
        }

        /// <summary>
        /// Return a EffectProperties struct instance initialized with parameters or default values.
        /// </summary>
        /// <param name="priority">Level of priority [1; 10] (default - 5). In case too
        /// many effects are playing simultaneously, the effect with lowest
        /// priority(10) will be muted.</param>
        /// <param name="volume">Percentage of the base volume between [0; 250]% (default - 100): [0;100[% the pattern attenuated,
        /// 100% the pattern's base volume is preserved, ]100; 250]% the pattern is amplified.
        /// Too much amplification may lead to the clipping of the haptic effects, distorting them
        /// and producing audible artifacts.</param>
        /// <param name="speed">Time scale between [0.01; 100] (default - 1): [0.01; 1[ the pattern is slowed down, 1 the pattern
        /// timing is preserved, ]1; 100] the pattern is accelerated.The resulting speed between
        /// the haptic effect's and the samples' speed within the pattern cannot exceed these
        /// bounds. Slowing down or accelerating a sample too much may result in an haptically poor effect.</param>
        /// <param name="repeatCount">Number of repetition of the pattern (default - 1) if the maxDuration is not reached.
        /// If set to 0, the pattern is repeat indefinitely until it is either stopped with stopEffect()
        /// or reach the maxDuration value.</param>
        /// <param name="repeatDelay">Pause in second between to repetition of the pattern (default - 0). This value is not
        /// affected by the speed parameter.</param>
        /// <param name="playAtTime">Time in the pattern at which the effect start to play (default - 0). This value need to be
        /// lower than the maxDuration.It also takes into account the repeatCount and the
        /// repeatDelay of the pattern.</param>
        /// <param name="maxDuration">Maximum duration of the effect (default - 0), it is automatically stopped if the duration
        /// is reached without any regards for the actual state of the repeatCount.A maxDuration
        /// of 0 remove the duration limit, making the effect ables to play indefinitely.</param>
        /// <param name="effectBoost">Boost intensity level percent [-100; 100] (default - 0) of the effect to use instead of the
        /// default pattern value if overridePatternBoost is set to true. By using a negative value, can decrease or
        /// even nullify the global intensity boost set by the user.</param>
        /// <param name="overridePatternBoost">By setting this boolean to true (default - false), the effect will use the
        /// effectBoost value instead of the default pattern value.</param>
        /// <param name="height">Normalized height [-1; 1] to translate the pattern by (default - 0). A positive value translate
        /// the pattern upwards.</param>
        /// <param name="heading">Heading angle (in degree) to rotate the pattern by in the horizontal plan (default - 0). A positive
        /// value rotates the pattern to the left of the vest.</param>
        /// <param name="tilting">Tilting angle (in degree) to rotate the pattern by in the sagittal plan (default - 0). A positive
        /// value rotates the pattern upwards from front to back.</param>
        /// <param name="frontBackInversion">Invert the direction of the pattern on the front-back axis (default - false). Can be combine with other
        /// inversion or addition.</param>
        /// <param name="upDownInversion">Invert the direction of the pattern on the up-down axis (default - false). Can be combine with other
        /// inversion or addition.</param>
        /// <param name="rightLeftInversion">Invert the direction of the pattern on the right-left axis (default - false). Can be combine with other
        /// inversion or addition.</param>
        /// <param name="frontBackAddition">Perform a front-back addition of the pattern on the front-back axis (default - false). Overrides the
        /// frontBackInversion.Can be combine with other inversion or addition.</param>
        /// <param name="upDownAddition">Perform a up-down addition of the pattern on the front-back axis (default - false). Overrides the
        /// upDownInversion.Can be combine with other inversion or addition.</param>
        /// <param name="rightLeftAddition">Perform a right-left addition of the pattern on the front-back axis (default - false). Overrides the
        /// rightLeftInversion.Can be combine with other inversion or addition.</param>
        /// <returns></returns>
        public static SkineticSDK.EffectProperties InitEffectProperties(int priority = 5,
                                                                        float volume = 100,
                                                                        float speed = 1,
                                                                        int repeatCount = 1,
                                                                        float repeatDelay = 0,
                                                                        float playAtTime = 0,
                                                                        float maxDuration = 0,
                                                                        int effectBoost = 0,
                                                                        bool overridePatternBoost = false,
                                                                        float height = 0,
                                                                        float heading = 0,
                                                                        float tilting = 0,
                                                                        bool frontBackInversion = false,
                                                                        bool upDownInversion = false,
                                                                        bool rightLeftInversion = false,
                                                                        bool frontBackAddition = false,
                                                                        bool upDownAddition = false,
                                                                        bool rightLeftAddition = false)
        {
            return new SkineticSDK.EffectProperties(priority, volume, speed, repeatCount, repeatDelay, playAtTime, maxDuration, effectBoost, overridePatternBoost,
                                                        height, heading, tilting, frontBackInversion, upDownInversion, rightLeftInversion,
                                                        frontBackAddition, upDownAddition, rightLeftAddition);
        }

        /// <summary>
        /// <c>Stops the effects instance identified by its ID.</c>
        /// </summary>
        /// <param name="effectID">ID of the effect to stop. (Effect ID is given at play)</param>
        /// <param name="fadeOut">Time in seconds before the effect stops [0s; 3.0s]</param>
        public void Stop(int effectID, float fadeOut)
        {
            _skineticInstance.StopEffect(effectID, fadeOut);
        }

        /// <summary>
        /// Enable the effect accumulation strategy: Whenever an effect is triggered on the primary pattern,
        /// the fallback one is used instead.
        /// The purpose of this feature is to enable the use of a lighter pattern if a specific pattern might
        /// be called too many time.Instead of triggering the mute strategy several time in a row, it enables
        /// the main effect to be rendered fully while having another lighter being triggered and still providing
        /// the additional event.
        ///
        /// The accumulation is done on the specified time window, starting with the first call. Each subsequent call
        /// within this time window will trigger the fallback pattern and increase the count until the maxAccumulation
        /// value is reach. Additional call will then be ignore until the time window is finished.
        /// If the subsequent call have the same or higher priority, the actual priority is set to be just below the main
        /// effect, as to preserve the unity of the accumulation with respect to other playing effects. However, if the
        /// subsequent calls have lower priorities, then the standard priority system is used.
        ///
        /// If a new call to this function is done for a specific pattern, the previous association is overridden.
        /// </summary>
        /// <param name="primaryPattern">the enum value of the main pattern.</param>
        /// <param name="fallbackPattern">the enum value of the fallback pattern</param>
        /// <param name="timeWindow">the time window during which the accumulation should happen.</param>
        /// <param name="maxAccumulation">max number of extra accumulated effect instances.</param>
        /// <returns>0 on success, a SkineticError code otherwise.</returns>
        public int SetFallbackPattern(Patterns primaryPattern, Patterns fallbackPattern, float timeWindow, int maxAccumulation)
        {
            if (_patternsIds.ContainsKey(primaryPattern) && _patternsIds.ContainsKey(fallbackPattern))
            {
                return _skineticInstance.SetAccumulationWindowToPattern(_patternsIds[primaryPattern], _patternsIds[fallbackPattern], timeWindow, maxAccumulation);
            }
            Plugin.Log?.Debug("One or more pattern not found: " + primaryPattern + " / " + fallbackPattern);
            return -1;
        }

        /// <summary>
        /// Disable the effect accumulation strategy on a specific pattern if any set.
        /// </summary>
        /// <param name="primaryPattern">the enum value of the main pattern.</param>
        /// <returns>0 on success, a SkineticError code otherwise.</returns>
        public int EraseFallbackPattern(Patterns primaryPattern)
        {
            if (_patternsIds.ContainsKey(primaryPattern))
            {
                return _skineticInstance.EraseAccumulationWindowToPattern(_patternsIds[primaryPattern]);
            }
            Plugin.Log?.Debug("Pattern not found: " + primaryPattern);
            return -1;
        }

        /// <summary>
        /// <c>LoadPatterns</c> method checks if the pattern folder already exists.
        /// If not, it creates it and fills it with the patterns predefined for the mod.
        /// Then, it loads the patterns from the disk into the SDK.
        /// Thus, it allows the end user to change the patterns according to their needs.
        /// </summary>
        private void LoadPatterns()
        {
            string fullPatternsPath = Directory.GetCurrentDirectory() + @"\" + _skineticPatternsFolder;
            if (!Directory.Exists(fullPatternsPath))
            {
                Plugin.Log?.Debug("Skinetic patterns folder not found. Attempt to create folder and patterns at '" + fullPatternsPath + "'");
                CreatePatternFiles(fullPatternsPath);
            }

            LoadPattern(Patterns.Connect, "SkineticConnected");
            LoadPattern(Patterns.PerfectNote, "SlicingOK");
            LoadPattern(Patterns.ImperfectNote, "SlicingNOTOK");
            LoadPattern(Patterns.Bomb, "Bomb");
            LoadPattern(Patterns.X, "_8X");
            LoadPattern(Patterns.Missed, "Missed");

            LoadPattern(Patterns.LevelFailed, "Failed");
            LoadPattern(Patterns.LevelCleared, "Cleared");

            LoadPattern(Patterns.UIHover, "UI_Hover");
            LoadPattern(Patterns.UIClick, "UI_Click");

            LoadPattern(Patterns.Wall | Patterns.Stay, "WallStay");
            LoadPattern(Patterns.Wall | Patterns.Hit, "WallHit");

            LoadPattern(Patterns.Kick, "Kick");
            LoadPattern(Patterns.Spin, "Spin");
            LoadPattern(Patterns.LightBoost, "LightBoost");
            LoadPattern(Patterns.H1, "hat1");
            LoadPattern(Patterns.H2, "hat2");
            LoadPattern(Patterns.H3, "hat3");
            LoadPattern(Patterns.H4, "hat4");
            LoadPattern(Patterns.H5, "hat5");
            LoadPattern(Patterns.H6, "hat6");
            LoadPattern(Patterns.H7, "hat7");
            LoadPattern(Patterns.H8, "hat8");

            LoadPattern(Patterns.Arc, "SaberArc");

            // To load new pattern in SkineticSDK:
            //LoadPattern(Patterns.[NewValue], [spn file name without extension]);
        }

        /// <summary>
        /// <c>LoadPattern</c> method loads the content of the pattern file into the SDK and store its patternID in the PatternIds hashmap.
        /// </summary>
        /// <param name="patternId">Pattern enum value to store the pattern ID in the hashmap</param>
        /// <param name="patternFileName">Name of the spn file to load</param>
        private void LoadPattern(Patterns patternId, string patternFileName)
        {
            Plugin.Log?.Debug("--- " + patternId + " | " + patternFileName);
            string fullPath = _skineticPatternsFolder + "\\" + patternFileName + ".spn";
            if (!File.Exists(fullPath))
                return;
            Plugin.Log?.Debug("Skinetic - load pattern: " + patternId + " | " + patternFileName);
            string spnContent = File.ReadAllText(fullPath);
            _patternsIds.Add(patternId, _skineticInstance.LoadPatternFromJSON(spnContent));
        }

        /// <summary>
        /// <c>CreatePatternFiles</c> method creates a folder at the given path and fill it with the patterns predefined for the mod.
        /// </summary>
        /// <param name="patternPath"></param>
        private void CreatePatternFiles(string patternPath)
        {
            Directory.CreateDirectory(patternPath);

            ResourceSet res = Skinetic_BeatSaber_Mod.Properties.Resources.ResourceManager.GetResourceSet(CultureInfo.InvariantCulture, true, true);
            foreach (DictionaryEntry dict in res)
            {
                var filePath = patternPath + "\\" + dict.Key + ".spn";
                File.WriteAllText(filePath, dict.Value.ToString());
            }
        }

        /// <summary>
        /// Delegate example of the connection callback.
        /// Will log the status of the connection.
        /// The callback is fired at the end of the connection routine weither it succeed
        /// or failed. It is also fired if a connection issue arise.
        /// </summary>
        /// <param name="state"> status of the connection</param>
        /// <param name="error"> Code of error occurring</param>
        /// <param name="serialNumber"> Serial number of the device firing the callback</param>
        public void ConnectionCallback(SkineticSDK.ConnectionState state, int error, System.UInt32 serialNumber)
        {
            string connectString = " wrong status ";

            if (error < 0)
            {
                connectString = "Device disconnected: " + SkineticSDK.getSDKError(0);
            }

            if (state == SkineticSDK.ConnectionState.E_CONNECTED)
            {
                connectString = "Device Connected = " + serialNumber;
            }
            else if (state == SkineticSDK.ConnectionState.E_DISCONNECTED)
            {
                connectString = "Device Disconnected";
            }

            Plugin.Log?.Debug("--- " + connectString + " ---");
        }

        /// <summary>
        /// Routine for automatic reconnection of the vest in the event of a disconnection
        /// </summary>
        /// <param name="manager"> Skinetic Manager instance to lock during the process</param>
        /// <param name="skineticInstance">Instance of the SDK being used</param>
        public void ConnectionStatus(SkineticManager manager, SkineticSDK skineticInstance)
        {
            Plugin.Log?.Debug("Automatic reconnection routine started");
            while (true)
            {
                if (manager._slowDetectStatus)
                {
                    Thread.Sleep(1000);
                }
                else
                {
                    Thread.Sleep(200);
                }
                try
                {
                    ConnectionState status = skineticInstance.ConnectionStatus();
                    lock (manager)
                    {
                        switch (status)
                        {
                            case ConnectionState.E_CONNECTED:
                                manager._slowDetectStatus = true;
                                if (manager.vestStatus != ConnectionState.E_CONNECTED)
                                {
                                    string serialNumber = manager.GetSerialNumber();
                                    Plugin.Log?.Debug($"Game connected to {serialNumber}");
                                    manager.vestStatus = ConnectionState.E_CONNECTED;
                                }
                                break;
                            case ConnectionState.E_DISCONNECTED:
                                manager._slowDetectStatus = false;
                                if (manager.vestStatus != ConnectionState.E_DISCONNECTED)
                                {
                                    Plugin.Log?.Debug($"Vest disconnected");
                                    Thread.Sleep(4000);
                                    Plugin.Log?.Debug($"Trying reconnection");
                                    skineticInstance.Connect(SkineticSDK.OutputType.E_AUTODETECT, 0);
                                    manager.vestStatus = ConnectionState.E_DISCONNECTED;
                                }
                                break;
                            case ConnectionState.E_CONNECTING:
                                manager._slowDetectStatus = false;
                                if (manager.vestStatus != ConnectionState.E_CONNECTING)
                                {
                                    Plugin.Log?.Debug($"Vest Connecting");
                                    manager.vestStatus = ConnectionState.E_CONNECTING;
                                }
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    Plugin.Log?.Error("ERROR IN THE AUTOMATIC CONNECTION SCRIPT");
                    Plugin.Log?.Error(e.Message);
                }
            }
        }
    }
}
