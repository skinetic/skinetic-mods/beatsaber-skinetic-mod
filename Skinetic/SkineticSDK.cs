﻿/*
 * This file is part of Skinetic Mod - Template.
 *
 * Skinetic Mod Template is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Skinetic Mod Template is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Skinetic Mod Template.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Skinetic
{
    public class SkineticSDK
    {

        /** @name Skinetic API
         *  API used to control the haptic rendering.
         */
        /// <summary>
        /// Type of connection to device.
        /// </summary>
        /// <remarks>
        /// E_AUTODETECT will try all available type of connection in the following order:
        /// - Bluetooth
        /// - USB
        /// - WIFI
        /// </remarks>
        public enum OutputType
        {
            /// <summary>Try all available type of connection.</summary>
            E_AUTODETECT = 0,
            /// <summary>Bluetooth connection.</summary>
            E_BLUETOOTH = 1,
            /// <summary>USB connection.</summary>
            E_USB = 2,
            /// <summary>Wifi connection.</summary>
            E_WIFI = 3,
        }

        /// <summary>
        /// Type of device.
        /// </summary>
        public enum DeviceType
        {
            /// <summary>Type is Unknown or undefined.</summary>
            E_UNKNOWN = ~0x0,
            /// <summary>Skinetic Vest.</summary>
            E_SKINETICVEST = 0x01100101,
            /// <summary>HSD mk.II development kit.</summary>
            E_HSDMK2 = 0x0122FFFF
        }

        /// <summary>
        /// Connection state.
        /// </summary>
        public enum ConnectionState
        {

            /// <summary> Device connection was broken, trying to reconnect.</summary>
            E_RECONNECTING = 3,
            /// <summary> Device is disconnecting, releasing all resources.</summary>
            E_DISCONNECTING = 2,
            /// <summary> Connection to the device is being established, connection routine is active.</summary>
            E_CONNECTING = 1,
            /// <summary> Device is connected.</summary>
            E_CONNECTED = 0,
            /// <summary> Device is disconnected.</summary>
            E_DISCONNECTED = -1,
        }

        /// <summary>
        /// Effect's state.
        /// </summary>
        public enum EffectState
        {
            /// <summary>Effect is playing.</summary>
            E_PLAY = 2,
            /// <summary>Effect is muted.</summary>
            E_MUTE = 1,
            /// <summary>Effect is initialized and should play as soon as possible.</summary>
            E_INITIALIZED = 0,
            /// <summary>Effect is stopped.</summary>
            E_STOP = -1,
        }

        /// <summary>
        /// Log levels definitions.
        /// </summary>
        public enum LogLevel
        {
            /// <summary>Trace level</summary>
            E_TRACE = 0,
            /// <summary>Debug level</summary>
            E_DEBUG = 1,
            /// <summary>Info level</summary>
            E_INFO = 2,
            /// <summary>Warning level</summary>
            E_WARN = 3,
            /// <summary>Error level</summary>
            E_ERR = 4
        }

        /// <summary>
        /// Describe the error cause.
        /// </summary>
        /// <param name="error"> value</param>
        /// <returns>Error message.</returns>
        public static string getSDKError(int error)
        {
            switch (error)
            {
                case 0:
                    return "No Error";
                case -1:
                    return "Other";
                case -2:
                    return "Invalid parameter";
                case -3:
                    return "No device connected";
                case -4:
                    return "Output is not supported on this platform";
                case -5:
                    return "Invalid Json";
                case -6:
                    return "Device not reachable";
                case -7:
                    return "A priority command is waiting to be processed";
                case -8:
                    return "No available slot on the board";
                case -9:
                    return "No Skinetic instance created";
                case -10:
                    return "Received an invalid message";
                case -11:
                    return "Process is already running";
                case -12:
                    return "A device is already connected";
                case -13:
                    return "The initialization of the device has been interrupted";
                case -14:
                    return "Play was ignored due to overall trigger strategy";
                case -15:
                    return "PortAudio raised an error";
                case -16:
                    return "An error happened with the socket";
                case -17:
                    return "ASH-fx library raised an error";
                case -50:
                    return "Error in JNI layer";
                case -100:
                    return "Core Error: Invalid argument";
                case -99:
                    return "Core Error: Invalid spn";
                case -98:
                    return "Core Error: Invalid layout";
                case -97:
                    return "Core Error: ID already allocated";
                case -96:
                    return "Core Error: Invalid sequence ID";
                case -95:
                    return " Core Error: Invalid pattern ID";
                case -94:
                    return "Core Error: Pattern in use";
                case -93:
                    return "Core Error: Sequence already set to play";
                case -92:
                    return "Core Error: Invalid Operation";
                default:
                    return "Unknown error";
            }
        }

        /// <summary>Struct representing device information.</summary>
        public struct DeviceInfo
        {
            /// <summary> Available Output connection mode. </summary>
            public OutputType outputType;
            /// <summary> Device Serial Number. </summary>
            public System.UInt32 serialNumber;
            /// <summary> Device Type. </summary>
            public DeviceType deviceType;
            /// <summary> Device Version. </summary>
            public string deviceVersion;
        }


        /// <summary>
        /// Haptic Structure to describe how effect instances reproduce a pattern with variations.
        /// </summary>
        /// <remarks>
        /// The spatialization properties (height, heading and tilting) allows to apply
        /// the pattern on the haptic device at a different location by
        /// translating/rotating it or performing some inversion/addition.
        /// Notice that combining additions greatly increase the processing time of the
        /// transformation.If the pattern possesses too many shapes and keys, a perceptible delay might be induced.
        ///
        /// The three transformations are applied in this order: tilting, vertical rotation, vertical translation.
        /// The default position of a pattern is the one obtained when these three parameters are set to zero.
        /// The actual use of these 3 parameters depends on the default position of the pattern and the targeted interaction:
        /// e.g.; for a piercing shot, a heading between[-180; 180]° can be combined with a tilting between[-90; 90] when
        /// using a shape-based pattern centered in the middle of the torso; for a environmental effect,
        /// a heading between[-90; 90]° (or[-180; 180]°) can be combined with a tilting between
        /// [-180; 180]° (resp. [-180; 0]°) when using a pattern with shapes centered on the top, etc.
        /// There are no actual bounds to the angles as not to restrict the usage.
        /// Notice that actuator-based patterns cannot be transformed in this version.
        ///
        /// The global boost intensity is applied to every effects being rendered as to increase them evenly. However, some
        /// effects are by design stronger than others. Hence, they all have a default boost value in the.spn that is added
        /// to the global boost intensity, and which can be set to compensate the discrepancy of intensity across a set of
        /// patterns. Weaker effects can have a high default boost value while, already strong effects can have a negative
        /// default boost value as to prevent the global boost intensity set by the user to increase the perceived intensity
        /// too much.Note that the resulting boost value is clamp between 0 and 100.
        /// When an instance of an effect is being rendered, the default boost value of the pattern, the one set in the design
        /// process, is used.If the boolean overridePatternBoost is set to true, the passed value effectBoost is used instead
        /// of the default one.
        ///
        /// Since all effects cannot be rendered simultaneously, the least priority ones are muted until
        /// the more priority ones are stopped of finished rendering. Muted effects are still running,
        /// but not rendered.
        ///
        /// The priority order is obtain using the priority level: priority increase from 10 to 1. In case
        /// of equality, the number of required simultaneous samples is used to determine which effect has the highest
        /// priority: effects using less simultaneous samples have a higher priority.Again, if the number of required
        /// simultaneous samples is the same, the most recent effect has a higher priority.
        /// </remarks>
        [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 8)]
        public struct EffectProperties
        {
            /// <summary>Level of priority [1; 10] (default - 5). In case too
            /// many effects are playing simultaneously, the effect with lowest
            /// priority - 10 - will be muted.</summary>
            public int priority;
            /// <summary>Percentage of the base volume between [0; 250]% (default - 100): [0;100[% the pattern attenuated,
            /// 100% the pattern's base volume is preserved, ]100; 250]% the pattern is amplified.
            /// Too much amplification may lead to the clipping of the haptic effects, distorting them
            /// and producing audible artifacts.</summary>
            public float volume;
            /// <summary>Time scale between [0.01; 100] (default - 1): [0.01; 1[ the pattern is slowed down, 1 the pattern
            /// timing is preserved, ]1; 100] the pattern is accelerated. The resulting speed between
            /// the haptic effect's and the samples' speed within the pattern cannot exceed these
            /// bounds. Slowing down or accelerating a sample too much may result in an haptically poor effect.</summary>
            public float speed;
            /// <summary>Number of repetition of the pattern (default - 1) if the maxDuration is not reached.
            /// If set to 0, the pattern is repeat indefinitely until it is either stopped with stopEffect()
            /// or reach the maxDuration value.</summary>
            public int repeatCount;
            /// <summary>Pause in second between to repetition of the pattern (default - 0). This value is not
            /// affected by the speed parameter.</summary>
            public float repeatDelay;
            /// <summary>Time in the pattern at which the effect start to play (default - 0). This value need to be
            /// lower than the maxDuration.It also takes into account the repeatCount and the
            /// repeatDelay of the pattern.</summary>
            public float playAtTime;
            /// <summary>Maximum duration of the effect (default - 0), it is automatically stopped if the duration
            /// is reached without any regards for the actual state of the repeatCount.A maxDuration
            /// of 0 remove the duration limit, making the effect ables to play indefinitely.</summary>
            public float maxDuration;
            /// <summary>Boost intensity level percent [-100; 100] (default - 0) of the effect to use instead of the
            /// default pattern value if overridePatternBoost is set to true. By using a negative value, can decrease or
            /// even nullify the global intensity boost set by the user.</summary>
            public int effectBoost;
            /// <summary>By setting this boolean to true (default - false), the effect will use the
            /// effectBoost value instead of the default pattern value.</summary>
            [MarshalAs(UnmanagedType.I1)]
            public Boolean overridePatternBoost;
            /// <summary>Normalized height [-1; 1] to translate the pattern by (default - 0). A positive value translate
            /// the pattern upwards. Not applicable to actuator-based patterns.</summary>
            public float height;
            /// <summary>Heading angle (in degree) to rotate the pattern by in the horizontal plan (default - 0). A positive
            /// value rotates the pattern to the left of the vest. Not applicable to actuator-based patterns.</summary>
            public float heading;
            /// <summary>Tilting angle (in degree) to rotate the pattern by in the sagittal plan (default - 0). A positive
            /// value rotates the pattern upwards from front to back. Not applicable to actuator-based patterns.</summary>
            public float tilting;
            /// <summary>Invert the direction of the pattern on the front-back axis (default - false). Can be combine with other
            /// inversion or addition. Not applicable to actuator-based patterns.</summary>
            [MarshalAs(UnmanagedType.I1)]
            public Boolean frontBackInversion;
            /// <summary>Invert the direction of the pattern on the up-down axis (default - false). Can be combine with other
            /// inversion or addition. Not applicable to actuator-based patterns.</summary>
            [MarshalAs(UnmanagedType.I1)]
            public Boolean upDownInversion;
            /// <summary>Invert the direction of the pattern on the right-left axis (default - false). Can be combine with other
            /// inversion or addition. Not applicable to actuator-based patterns.</summary>
            [MarshalAs(UnmanagedType.I1)]
            public Boolean rightLeftInversion;
            /// <summary>Perform a front-back addition of the pattern on the front-back axis (default - false). Overrides the
            /// frontBackInversion.Can be combine with other inversion or addition. Not applicable to actuator-based patterns.</summary>
            [MarshalAs(UnmanagedType.I1)]
            public Boolean frontBackAddition;
            /// <summary>Perform a up-down addition of the pattern on the front-back axis (default - false). Overrides the
            /// upDownInversion.Can be combine with other inversion or addition. Not applicable to actuator-based patterns.</summary>
            [MarshalAs(UnmanagedType.I1)]
            public Boolean upDownAddition;
            /// <summary>Perform a right-left addition of the pattern on the front-back axis (default - false). Overrides the
            /// rightLeftInversion.Can be combine with other inversion or addition. Not applicable to actuator-based patterns.</summary>
            [MarshalAs(UnmanagedType.I1)]
            public Boolean rightLeftAddition;

            public EffectProperties(int priority, float volume, float speed, int repeatCount, float repeatDelay,
                                    float playAtTime, float maxDuration, int effectBoost, bool overridePatternBoost,
                                    float height, float heading, float tilting,
                                    bool frontBackInversion, bool upDownInversion, bool rightLeftInversion,
                                    bool frontBackAddition, bool upDownAddition, bool rightLeftAddition)
            {
                this.priority = priority;
                this.volume = volume;
                this.speed = speed;
                this.repeatCount = repeatCount;
                this.repeatDelay = repeatDelay;
                this.playAtTime = playAtTime;
                this.maxDuration = maxDuration;
                this.effectBoost = effectBoost;
                this.overridePatternBoost = overridePatternBoost;
                this.height = height;
                this.heading = heading;
                this.tilting = tilting;
                this.frontBackInversion = frontBackInversion;
                this.upDownInversion = upDownInversion;
                this.rightLeftInversion = rightLeftInversion;
                this.frontBackAddition = frontBackAddition;
                this.upDownAddition = upDownAddition;
                this.rightLeftAddition = rightLeftAddition;
            }
        }

        /// <summary>
        /// Experimental - Preset of audio devices.
        /// </summary>
        /// <remarks>
        /// E_CUSTOMDEVICE is to be used for a custom configuration.
        /// </remarks>
        public enum ExpAudioPreset
        {
            /// <summary>Audio stream with a custom configuration.</summary>
            E_CUSTOMDEVICE = 0,
            /// <summary>Autoconfiguration of the audioStream for the Skinetic device.</summary>
            E_SKINETIC = 1,
            /// <summary>Autoconfiguration of the audioStream for the HSD mk.I device.</summary>
            E_HSDMKI = 2,
            /// <summary>Autoconfiguration of the audioStream for the HSD mk.II device.</summary>
            E_HSDMKII = 3,
            /// <summary>Autoconfiguration of the audioStream for the HSD 0 device.</summary>
            E_HSD0 = 4,
        }

        /// <summary>Experimental - Struct containing settings for an audio connection.</summary>
        [System.Serializable]
        public struct ExpAudioSettings
        {
            /// <summary>Name of the targeted audio device, default value
            /// "default" uses the OS default audio device. If using a specific
            /// ExpAudioPreset other than eCustomDevice, the parameter will be
            /// ignored.</summary>
            public string deviceName;
            /// <summary>Name of the targeted API. Default value "any_API" uses
            /// any available API which match the configuration, if any.
            /// If using a specific ExpAudioPreset other than E_CUSTOMDEVICE,
            /// the parameter will be ignored.</summary>
            public string audioAPI;
            /// <summary>Sample rate of the audio stream. If using a specific
            /// ExpAudioPreset, the parameter will be ignored.</summary>
            public int sampleRate;
            /// <summary>Size (strictly positive) of a chunk of data sent over
            /// the audio stream. This parameter MUST be set independently
            /// of the used ExpAudioPreset.</summary>
            public int bufferSize;
            /// <summary>Number of channels (strictly positive) to use while
            /// streaming to the haptic output. If using a specific audio
            /// preset, the parameter will be ignored. Setting -1 will use the
            /// number of actuator of the layout, or a portion of it.</summary>
            public int nbStreamChannel;
            /// <summary>Desired latency in seconds. The value is rounded to the
            /// closest available latency value from the audio API. If using a
            /// specific ExpAudioPreset other than eCustomDevice, the parameter
            /// will be ignored.</summary>
            public float suggestedLatency;

            public ExpAudioSettings(string deviceName, string audioAPI, int sampleRate, int bufferSize, int nbStreamChannel, float suggestedLatency)
            {
                this.deviceName = deviceName;
                this.audioAPI = audioAPI;
                this.sampleRate = sampleRate;
                this.bufferSize = bufferSize;
                this.nbStreamChannel = nbStreamChannel;
                this.suggestedLatency = suggestedLatency;
            }
        }

        //! @cond
        [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 8)]
        private struct CDeviceInfo
        {
            /** Available Output connection mode.*/
            public OutputType outputType;
            /** Device Serial Number.*/
            public uint serialNumber;
            /** Device Type.*/
            public DeviceType deviceType;
            /** Device Version.*/
            [MarshalAs(UnmanagedType.LPStr)] public string deviceVersion;
            /** Pointer to next device.*/
            public IntPtr next;
        }

        [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 8)]
        private struct CAudioSettings
        {
            /** Name of the targeted audio device, default value "default_output" uses the OS default audio device.
             * If using a specific audioPreset other than eCustomDevice, the parameter will be ignored.*/
            [MarshalAs(UnmanagedType.LPStr)] public string deviceName;
            /** Name of the targeted API, default value "any_API" uses any available API.*/
            [MarshalAs(UnmanagedType.LPStr)] public string audioAPI;
            /** Sample rate of the audio stream. If using a specific audioPreset, the parameter will be ignored.*/
            public uint sampleRate;
            /** Size of a chunk of data sent over the audio stream.*/
            public uint bufferSize;
            /** Number of channels to use while streaming to the haptic output. If using a specific audio
             * preset, the parameter will be ignored. Setting -1 will use the number of actuator of the layout, or a portion of it.*/
            public int nbStreamChannel;
            /** Desired latency in seconds, default value "-1" sets it automatically. The value is rounded
             * to the closest available latency value from the audio API.*/
            public float suggestedLatency;

            public CAudioSettings(ExpAudioSettings audioSettings)
            {
                this.deviceName = audioSettings.deviceName;
                this.audioAPI = audioSettings.audioAPI;
                this.sampleRate = (UInt32)audioSettings.sampleRate;
                this.bufferSize = (UInt32)audioSettings.bufferSize;
                this.nbStreamChannel = audioSettings.nbStreamChannel;
                this.suggestedLatency = audioSettings.suggestedLatency;
            }
        }
        //! @endcond


        private const string DLLNAME = @".\Libs\SkineticSDK";

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void WrappingConnectionCallbackDelegate(ConnectionState status, int error, UInt32 serialNumber, IntPtr userData);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void WrappingLogCallbackDelegate(LogLevel level, [MarshalAs(UnmanagedType.LPStr)] string scope,
            [MarshalAs(UnmanagedType.LPStr)] string message);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_serialNumberToString")]
        private static extern IntPtr Ski_serialNumberToString(UInt32 serialNumber);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_createSDKInstance")]
        private static extern int Ski_createSDKInstance([MarshalAs(UnmanagedType.LPStr)] string logFileName);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_freeSDKInstance")]
        private static extern void Ski_freeSDKInstance(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_scanDevices")]
        private static extern int Ski_scanDevices(int sdk_ID, OutputType outputType);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_scanStatus")]
        private static extern int Ski_scanStatus(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getFirstScannedDevice")]
        private static extern IntPtr Ski_getFirstScannedDevice(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_connectDevice")]
        private static extern int Ski_connectDevice(int sdk_ID, OutputType outputType, UInt32 serialNumber);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_disconnectDevice")]
        private static extern int Ski_disconnectDevice(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_connectionStatus")]
        private static extern ConnectionState Ski_connectionStatus(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_setConnectionCallback")]
        private static extern int Ski_setConnectionCallback(int sdk_ID, IntPtr callback, IntPtr userData);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSDKVersion")]
        private static extern IntPtr Ski_getSDKVersion(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSkineticSerialNumber")]
        private static extern UInt32 Ski_getSkineticSerialNumber(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSkineticSerialNumberAsString")]
        private static extern IntPtr Ski_getSkineticSerialNumberAsString(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSkineticVersion")]
        private static extern IntPtr Ski_getSkineticVersion(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSkineticType")]
        private static extern DeviceType Ski_getSkineticType(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getGlobalIntensityBoost")]
        private static extern int Ski_getGlobalIntensityBoost(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_setGlobalIntensityBoost")]
        private static extern int Ski_setGlobalIntensityBoost(int sdk_ID, int globalBoost);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_loadPatternFromJSON")]
        private static extern int Ski_loadPatternFromJSON(int sdk_ID, [MarshalAs(UnmanagedType.LPStr)] string json);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_unloadPattern")]
        private static extern int Ski_unloadPattern(int sdk_ID, int patternID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getPatternIntensityBoost")]
        private static extern int Ski_getPatternIntensityBoost(int sdk_ID, int patternID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_setAccumulationWindowToPattern")]
        private static extern int Ski_setAccumulationWindowToPattern(int sdk_ID, int mainPatternID, int fallbackPatternID, float timeWindow, int maxAccumulation);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_eraseAccumulationWindowToPattern")]
        private static extern int Ski_eraseAccumulationWindowToPattern(int sdk_ID, int mainPatternID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_playEffect")]
        private static extern int Ski_playEffect(int sdk_ID, int patternID, EffectProperties effectProperties);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_stopEffect")]
        private static extern int Ski_stopEffect(int sdk_ID, int effectID, float time);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_effectState")]
        private static extern EffectState Ski_effectState(int sdk_ID, int effectI);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_pauseAll")]
        private static extern int Ski_pauseAll(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_resumeAll")]
        private static extern int Ski_resumeAll(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_stopAll")]
        private static extern int Ski_stopAll(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_setLogCallback")]
        private static extern int Ski_setLogCallback(IntPtr callback);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_enableLegacyBackend")]
        private static extern int Ski_exp_enableLegacyBackend(int sdk_ID, Boolean enable);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_connectAsh")]
        private static extern int Ski_exp_connectAsh(int sdk_ID, OutputType outputType, UInt32 serialNumber, [MarshalAs(UnmanagedType.LPStr)] string loopbackInterface);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_connectAudio")]
        private static extern int Ski_exp_connectAudio(int sdk_ID, ExpAudioPreset audioPreset, CAudioSettings audioSettings);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_connectAshAudio")]
        private static extern int Ski_exp_connectAshAudio(int sdk_ID, ExpAudioPreset audioPreset, CAudioSettings audioSettings, [MarshalAs(UnmanagedType.LPStr)] string loopbackInterface);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_setAshVolume")]
        private static extern int Ski_exp_setAshVolume(int sdk_ID, float volume);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_getAshVolume")]
        private static extern float Ski_exp_getAshVolume(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_getOutputDevicesNames", CallingConvention = CallingConvention.Cdecl)]
        private static extern int Ski_exp_getOutputDevicesNames(ref IntPtr devicesNames, ref int nbDevices);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_getLoopbackDevicesNames", CallingConvention = CallingConvention.Cdecl)]
        private static extern int Ski_exp_getLoopbackDevicesNames(ref IntPtr devicesNames, ref int nbDevices);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_getOutputDeviceAPIs")]
        private static extern int Ski_exp_getOutputDeviceAPIs([MarshalAs(UnmanagedType.LPStr)] string outputName, ref IntPtr apis, ref int nbApis);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_getOutputDeviceInfo")]
        private static extern int Ski_exp_getOutputDeviceInfo([MarshalAs(UnmanagedType.LPStr)] string outputName, [MarshalAs(UnmanagedType.LPStr)] string apiName, ref int max_channels, ref float default_low_latency, ref float default_high_latency);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_exp_getSupportedStandardSampleRates")]
        private static extern int Ski_exp_getSupportedStandardSampleRates([MarshalAs(UnmanagedType.LPStr)] string outputName, [MarshalAs(UnmanagedType.LPStr)] string api, ref IntPtr sampleRates, ref int nbSampleRates);


        private int m_instanceID = -1;
        private WrappingConnectionCallbackDelegate m_wrappingConnectionCallbackDelegate = ConnectionClassCallback;
        private ConnectionCallbackDelegate m_connectionDelegate = null;
        private GCHandle m_handle;

        public static void ConnectionClassCallback(ConnectionState status, int error, UInt32 serialNumber, IntPtr userData)
        {
            GCHandle obj = GCHandle.FromIntPtr(userData);
            ((SkineticSDK)obj.Target).ConnectionInstanceCallback(status, error, serialNumber);
        }

        private void ConnectionInstanceCallback(ConnectionState status, int error, UInt32 serialNumber)
        {
            m_connectionDelegate(status, error, serialNumber);
        }

        private static WrappingLogCallbackDelegate m_wrappingLogCallbackDelegate = LogCallback;
        private static LogCallbackDelegate m_logCallback = null;

        public static void LogCallback(LogLevel level, string scope, string message)
        {
            m_logCallback(level, scope, message);
        }

        /// <summary>
        /// Convert serial number to a formatted string.
        /// </summary>
        /// <param name="serialNumber">serial number to convert</param>
        /// <returns>string representation of the serial number</returns>
        public static String GetDeviceSerialNumberAsString(UInt32 serialNumber)
        {
            IntPtr ptr = Ski_serialNumberToString(serialNumber);
            // assume returned string is utf-8 encoded
            return Marshal.PtrToStringAnsi(ptr);
        }

        /// <summary>
        /// Initialize the device instance.
        /// </summary>
        public void InitInstance()
        {
            if (m_instanceID != -1)
                return;
            m_instanceID = Ski_createSDKInstance("");
            m_handle = GCHandle.Alloc(this, GCHandleType.Normal);
        }

        /// <summary>
        /// Deinitialize the device instance.
        /// </summary>
        public void DeinitInstance()
        {
            if (m_instanceID == -1)
                return;
            Ski_freeSDKInstance(m_instanceID);
            m_instanceID = -1;
            m_handle.Free();
        }

        /// <summary>
        /// Initialize a scanning routine to find all available devices.
        /// </summary>
        /// <remarks>
        /// The state of the routine can be obtain from ScanStatus(). Once completed,
        /// the result can be accessed using GetScannedDevices().
        /// </remarks>
        /// <param name="output"></param>
        /// <returns>0 on success, an Error code on failure.</returns>
        public int ScanDevices(OutputType output)
        {
            return Ski_scanDevices(m_instanceID, output);
        }

        /// <summary>
        /// Check the status of the asynchronous scanning routine.
        /// </summary>
        /// <remarks>
        /// The method returns:
        ///  - '1' if the scan is ongoing.
        ///  - '0' if the scan is completed.
        ///  - a negative error code if the connection failed.
        /// The asynchronous scan routine is terminated on failure.
        /// Once the scan is completed, the result can be obtain by calling GetScannedDevices().
        /// </remarks>
        /// <returns>the current status or an error on failure.</returns>
        public int ScanStatus()
        {
            return Ski_scanStatus(m_instanceID);
        }

        /// <summary>
        /// This function returns a list of all devices found during the scan
        /// which match the specified output type.
        /// </summary>
        /// <returns>a list of devices, empty if no match or an error occurs.</returns>
        public List<DeviceInfo> GetScannedDevices()
        {
            List<DeviceInfo> listDevices = new List<DeviceInfo>();
            IntPtr devicePtr = Ski_getFirstScannedDevice(m_instanceID);
            if (devicePtr == IntPtr.Zero)
            {
                return listDevices;
            }

            while (devicePtr != IntPtr.Zero)
            {
#if NET451_OR_GREATER
                // For .Net 4.5.1 and >
                CDeviceInfo cdevice = Marshal.PtrToStructure<CDeviceInfo>(devicePtr);
#else
                // For .Net < 4.5 use this instead
                CDeviceInfo cdevice = (CDeviceInfo)Marshal.PtrToStructure(devicePtr, typeof(CDeviceInfo));
#endif
                DeviceInfo device = new DeviceInfo();

                device.deviceType = cdevice.deviceType;
                device.deviceVersion = cdevice.deviceVersion;
                device.serialNumber = cdevice.serialNumber;
                device.outputType = cdevice.outputType;
                listDevices.Add(device);
                devicePtr = cdevice.next;
            }
            return listDevices;
        }

        /// <summary>
        /// Initialize an asynchronous connection to a device using the selected type of connection.
        /// </summary>
        /// <remarks>
        /// The state of the routine can be obtain from ConnectionStatus().
        /// If the serial number is set to '0', the connection will be performed on the first found device.
        /// </remarks>
        /// <param name="output">output type</param>
        /// <param name="serialNumber">serial number of the device to connect to</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Connect(OutputType output, System.UInt32 serialNumber)
        {
            return Ski_connectDevice(m_instanceID, output, serialNumber);
        }

        /// <summary>
        /// Disconnect the current device.
        /// </summary>
        /// <remarks>
        /// The disconnection is effective once all resources are released.
        /// The state of the routine can be obtain from ConnectionStatus().
        /// </remarks>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Disconnect()
        {
            return Ski_disconnectDevice(m_instanceID);
        }

        /// <summary>
        /// Check the current status of the connection.
        /// The asynchronous connection routine is terminated on failure.
        /// </summary>
        /// <returns>the current status of the connection.</returns>
        public ConnectionState ConnectionStatus()
        {
            return Ski_connectionStatus(m_instanceID);
        }

        /// <summary>
        /// Delegate of the connection callback.
        /// </summary>
        /// <remarks>
        /// Functions of type ski_ConnectionCallback are implemented by clients.
        /// The callback is fired at the end of the connection routine whether it succeeds
        /// or failed.
        /// It is also fired if a connection issue arise.
        /// </remarks>
        /// <param name="state"> status of the connection</param>
        /// <param name="error"> of error occurring</param>
        /// <param name="serialNumber"> serial number of the device firing the callback</param>
        public delegate void ConnectionCallbackDelegate(ConnectionState state, int error, System.UInt32 serialNumber);

        /// <summary>
        /// Delegate of the log callback.
        /// </summary>
        /// <remarks>
        /// Functions of type ski_LogCallback are implemented by clients.
        /// The callback is fired for each logging entry.
        /// </remarks>
        /// <param name="level"> level of the log message</param>
        /// <param name="scope"> string giving information about the SDK scope that send the log</param>
        /// <param name="message"> string containing the log message</param>
        public delegate void LogCallbackDelegate(LogLevel level, string scope, string message);

        /// <summary>
        /// Set a callback function fired upon connection changes.
        /// </summary>
        /// <remarks>
        /// Functions of type ski_ConnectionCallback are implemented by clients.
        ///
        /// The callback is fired at the end of the connection routine whether it succeed
        /// or failed. It is also fired if a connection issue arise.
        /// The callback is not fired if none was passed to setConnectionCallback().
        ///
        /// 'userData' is a client supplied pointer which is passed back when the callback
        /// function is called.It could for example, contain a pointer to an class instance
        /// that will process the callback.
        /// </remarks>
        /// <param name="callback">client's callback</param>
        /// <returns>0 on success, an Error code on failure.</returns>
        public int SetConnectionCallback(ConnectionCallbackDelegate callback)
        {
            int ret = Ski_setConnectionCallback(m_instanceID, Marshal.GetFunctionPointerForDelegate(m_wrappingConnectionCallbackDelegate), GCHandle.ToIntPtr(m_handle));
            if (ret == 0)
                m_connectionDelegate = callback;
            return ret;
        }

        public static int SetLogCallback(LogCallbackDelegate callback)
        {
            int ret = Ski_setLogCallback(Marshal.GetFunctionPointerForDelegate(m_wrappingLogCallbackDelegate));
            if (ret == 0)
                m_logCallback = callback;
            return 0;
        }

        /// <summary>
        /// Get SDK version as a string.
        /// </summary>
        /// <remarks>
        /// The format of the string is: <pre>major.minor.revision</pre>
        /// </remarks>
        /// <returns>The version string.</returns>
        public String GetSDKVersion()
        {
            IntPtr ptr = Ski_getSDKVersion(m_instanceID);
            // assume returned string is utf-8 encoded
            return Marshal.PtrToStringAnsi(ptr);
        }

        /// <summary>
        /// Get the connected device's version as a string.
        /// </summary>
        /// <remarks>
        /// The format of the string is: <pre>major.minor.revision</pre>
        /// </remarks>
        /// <returns>The version string if a device is connected,
        /// an error message otherwise.</returns>
        public String GetDeviceVersion()
        {
            IntPtr ptr = Ski_getSkineticVersion(m_instanceID);
            // assume returned string is utf-8 encoded
            return Marshal.PtrToStringAnsi(ptr);
        }

        /// <summary>
        /// Get the connected device's serial number.
        /// </summary>
        /// <returns>The serial number of the connected 
        /// device if any, 0xFFFFFFFF otherwise.</returns>
        public System.UInt32 GetDeviceSerialNumber()
        {
            return Ski_getSkineticSerialNumber(m_instanceID);
        }

        /// <summary>
        /// Get the connected device's serial number as string.
        /// </summary>
        /// <returns>The serial number as string of the connected device
        /// if any, "noDeviceConnected" otherwise.</returns>
        public String GetDeviceSerialNumberAsString()
        {
            IntPtr ptr = Ski_getSkineticSerialNumberAsString(m_instanceID);
            // assume returned string is utf-8 encoded
            return Marshal.PtrToStringAnsi(ptr);
        }

        /// <summary>
        /// Get the connected device's type.
        /// </summary>
        /// <returns>The type of the connected device if it is connected,
        /// an ERROR message otherwise.</returns>
        public DeviceType GetDeviceType()
        {
            return Ski_getSkineticType(m_instanceID);
        }

        /// <summary>
        /// Get the amount of effect's intensity boost.
        /// </summary>
        /// <remarks>
        /// The boost increase the overall intensity of all haptic effects.
        /// However, the higher the boost activation is, the more the haptic effects are degraded.
        /// The global boost is meant to be set by the user as an application setting.
        /// </remarks>
        /// <returns>The percentage of effect's intensity boost, an ERROR otherwise.</returns>
        public int GetGlobalIntensityBoost()
        {
            return Ski_getGlobalIntensityBoost(m_instanceID);
        }

        /// <summary>
        /// Set the amount of global intensity boost.
        /// </summary>
        /// <remarks>
        /// The boost increase the overall intensity of all haptic effects.
        /// However, the higher the boost activation is, the more the haptic effects are degraded.
        /// The global boost is meant to be set by the user as an application setting.
        /// </remarks>
        /// <param name="globalBoost">boostPercent percentage of the boost.</param>
        /// <returns>0 on success, an ERROR otherwise.</returns>
        public int SetGlobalIntensityBoost(int globalBoost)
        {
            return Ski_setGlobalIntensityBoost(m_instanceID, globalBoost);
        }

        /// <summary>
        /// Load a pattern from a valid json into a local haptic asset and return
        /// the corresponding patternID.
        /// </summary>
        /// <remarks>
        /// The patternID is a positive index.
        /// </remarks>
        /// <param name="json">describing the pattern</param>
        /// <returns>Positive patternID on success, an error otherwise.</returns>
        public int LoadPatternFromJSON(String json)
        {
            return Ski_loadPatternFromJSON(m_instanceID, json);
        }

        /// <summary>
        /// Unload the pattern from of the corresponding patternID.
        /// </summary>
        /// <param name="patternID">the patternID of the pattern to unload.</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int UnloadPattern(int patternID)
        {
            return Ski_unloadPattern(m_instanceID, patternID);
        }

        /// <summary>
        ///Get the pattern boost value which serves as a default value for the playing effect.
        /// </summary>
        /// <remarks>
        ///The value is ranged in [-100; 100].
        ///If the pattern ID is invalid, zero is still returned.
        /// </remarks>
        /// <param name="patternID">the ID of the targeted pattern.</param>
        /// <returns>the pattern intensity boost of the pattern if it exists, 0 otherwise.</returns>
        public int GetPatternIntensityBoost(int patternID)
        {
            return Ski_getPatternIntensityBoost(m_instanceID, patternID);
        }

        /// <summary>
        /// Enable the effect accumulation strategy on a targeted pattern.
        /// </summary>
        /// <remarks>
        /// Whenever an effect is triggered on the main pattern,
        /// the fallback one is used instead, if the main is already playing. More details can be found the
        /// additional documentation.
        /// For the maxAccumulation, setting to 0 removes the limit.
        ///
        /// If a new call to this function is done for a specific pattern, the previous association is overridden.
        /// </remarks>
        /// <param name="mainPatternID">the patternID of the main pattern.</param>
        /// <param name="fallbackPatternID">the patternID of the fallback pattern</param>
        /// <param name="timeWindow">the time window during which the accumulation should happen.</param>
        /// <param name="maxAccumulation">max number of extra accumulated effect instances.</param>
        /// <returns>0 on success, an ERROR otherwise.</returns>
        public int SetAccumulationWindowToPattern(int mainPatternID, int fallbackPatternID, float timeWindow, int maxAccumulation)
        {
            return Ski_setAccumulationWindowToPattern(m_instanceID, mainPatternID, fallbackPatternID, timeWindow, maxAccumulation);
        }

        /// <summary>
        /// Disable the effect accumulation strategy on a specific pattern if any set.
        /// </summary>
        /// <param name="mainPatternID">the patternID of the main pattern.</param>
        /// <returns>0 on success, an ERROR otherwise.</returns>
        public int EraseAccumulationWindowToPattern(int mainPatternID)
        {
            return Ski_eraseAccumulationWindowToPattern(m_instanceID, mainPatternID);
        }

        /// <summary>
        /// Play an haptic effect based on a loaded pattern and return the effectID of this instance.
        /// </summary>
        /// <remarks>
        /// The instance index is positive. Each call to playEffect() using the same patternID
        /// generates a new haptic effect instance totally uncorrelated to the previous ones.
        /// The instance is destroyed once it stops playing.
        ///
        /// The haptic effect instance reproduces the pattern with variations describes in the structure
        /// ski_effect_properties_t.More information on these parameters and how to used them can be found
        /// in the structure's description. Transformation and boolean operations are not applicable to
        /// actuator-based patterns.
        ///
        /// If the pattern is unloaded, the haptic effect is not interrupted.
        /// </remarks>
        /// <param name="patternID">pattern used by the effect instance.</param>
        /// <param name="effectProperties">struct to specialized the effect.</param>
        /// <returns>Positive effectID on success, an error otherwise.</returns>
        public int PlayEffect(int patternID, EffectProperties effectProperties)
        {
            return Ski_playEffect(m_instanceID, patternID, effectProperties);
        }

        /// <summary>
        /// Stop the effect instance identified by its effectID.
        /// </summary>
        /// <remarks>
        /// The effect is stop in "time" seconds with a fade out to prevent abrupt transition. If time
        /// is set to 0, no fadeout are applied and the effect is stopped as soon as possible.
        /// Once an effect is stopped, it is instance is destroyed and its effectID invalidated.
        /// </remarks>
        /// <param name="effectID">index identifying the effect.</param>
        /// <param name="time">duration of the fadeout in second.</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int StopEffect(int effectID, float time)
        {
            return Ski_stopEffect(m_instanceID, effectID, time);
        }

        /// <summary>
        ///  Get the current state of an effect.
        /// </summary>
        /// <remarks>
        /// If the effectID is invalid, the 'stop' state will be return.
        /// </remarks>
        /// <param name="effectID">index identifying the effect.</param>
        /// <returns>the current state of the effect.</returns>
        public EffectState GetEffectState(int effectID)
        {
            return Ski_effectState(m_instanceID, effectID);
        }

        /// <summary>
        /// Pause all haptic effect that are currently playing.
        /// </summary>
        /// <returns>0 on success, an error otherwise.</returns>
        public int PauseAll()
        {
            return Ski_pauseAll(m_instanceID);
        }

        /// <summary>
        /// Resume the paused haptic effects.
        /// </summary>
        /// <returns>0 on success, an error otherwise.</returns>
        public int ResumeAll()
        {
            return Ski_resumeAll(m_instanceID);
        }

        /// <summary>
        /// Stop all playing haptic effect.
        /// </summary>
        /// <returns>0 on success, an error otherwise.</returns>
        public int StopAll()
        {
            return Ski_stopAll(m_instanceID);
        }
        ///@}

        /** @name Experimental
         *  Experimental API that allows to test experimental features.
         */
        ///@{
        /// <summary>
        /// Enable legacy backend.
        /// </summary>
        /// <remarks>
        /// If boolean is set to true, the legacy backend is used instead of the
        /// default backend.
        /// </remarks>
        /// <param name="enable">set to true to enable, false otherwise.</param>
        public void Exp_EnableLegacyBackend(bool enable)
        {
            Ski_exp_enableLegacyBackend(m_instanceID, enable);
        }

        /// <summary>
        /// Initialize an asynchronous connection to a device and use
        /// the ASH-fx library for haptic generation.
        /// </summary>
        /// <remarks>
        /// The ASH-fx library generates haptic signals based on the audio of the targeted 
        /// input loopback interface. 
        /// The loopback interfaces can be queried by calling getLoopbackDevicesNames(). 
        /// Setting loopbackInterface to NULL selects the default audio output device 
        /// of the system.
        /// </remarks>
        /// <param name="output">output type</param>
        /// <param name="serialNumber">serial number of the device to connect to</param>
        /// <param name="loopbackInterface">input loopback interface</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Exp_ConnectAsh(OutputType output, System.UInt32 serialNumber, string loopbackInterface)
        {
            return Ski_exp_connectAsh(m_instanceID, output, serialNumber, loopbackInterface);
        }

        /// <summary>
        /// Initialize an asynchronous connection to an audio device using the
        /// provided settings.
        /// </summary>
        /// <remarks>
        /// If audioPreset is set to anything else other than
        /// ExpAudioPreset::E_CUSTOMDEVICE, the provided settings are ignored and
        /// the ones corresponding to the preset are used instead.
        /// Notice that this connection is not compatible with the legacy backend.
        /// </remarks>
        /// <param name="audioPreset">preset of audio device.</param>
        /// <param name="audioSettings">stream settings</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Exp_ConnectAudio(ExpAudioPreset audioPreset, ExpAudioSettings audioSettings)
        {
            CAudioSettings cAudioSettings = new CAudioSettings(audioSettings);
            return Ski_exp_connectAudio(m_instanceID, audioPreset, cAudioSettings);
        }

        /// <summary>
        /// Initialize an asynchronous connection to an audio device and use
        /// the ASH-fx library for haptic generation.
        /// </summary>
        /// <remarks>
        /// The ASH-fx library generates haptic signals based on the audio of the targeted 
        /// input loopback interface. 
        /// The loopback interfaces can be queried by calling getLoopbackDevicesNames(). 
        /// Setting loopbackInterface to NULL selects the default audio output device 
        /// of the system.
        /// If audioPreset is set to anything else other than
        /// ExpAudioPreset::E_CUSTOMDEVICE, the provided settings are ignored and
        /// the ones corresponding to the preset are used instead.
        /// </remarks>
        /// <param name="audioPreset">preset of audio device.</param>
        /// <param name="audioSettings">stream settings</param>
        /// <param name="loopbackInterface">input loopback interface</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Exp_ConnectAshAudio(ExpAudioPreset audioPreset, ExpAudioSettings audioSettings, string loopbackInterface)
        {
            CAudioSettings cAudioSettings = new CAudioSettings(audioSettings);
            return Ski_exp_connectAshAudio(m_instanceID, audioPreset, cAudioSettings, loopbackInterface);
        }

        /// <summary>
        /// Set the volume of the ASH-generated haptic track.
        /// </summary>
        /// <param name="volume">haptic normalized volume</param>
        /// <remarks>
        /// The volume is normalized and ranged between [0; 1]. Values above 1 can be used 
        /// but might produce clipping.
        /// </remarks>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Exp_SetAshVolume(float volume)
        {
            return Ski_exp_setAshVolume(m_instanceID, volume);
        }

        /// <summary>
        /// Get the volume of the ASH-generated haptic track.
        /// </summary>
        /// <returns>positive normalized haptic volume, an Error code on failure.</returns>
        public float Exp_GetAshVolume()
        {
            return (float)Ski_exp_getAshVolume(m_instanceID);
        }

        /// <summary>
        /// Get names of available audio output devices.
        /// </summary>
        /// <remarks>
        /// If no device is available, the array will contain "noDevice".
        /// This allows to select which device to use if initializing the
        /// context with eCustomDevice.
        /// </remarks>
        /// <returns>array of device names</returns>
        public static string[] Exp_GetOutputDevicesNames()
        {
            string[] output = new string[0];
            int nbOutput = 0;
            IntPtr arrPtr = IntPtr.Zero;
            int ret = Ski_exp_getOutputDevicesNames(ref arrPtr, ref nbOutput);
            if (ret < 0)
                return output;
            if (arrPtr == IntPtr.Zero)
            {
                return output;
            }
            IntPtr[] managedArray = new IntPtr[nbOutput];
            Marshal.Copy(arrPtr, managedArray, 0, nbOutput);
            output = new string[nbOutput];
            for (int i = 0; i < nbOutput; i++)
            {
                output[i] = Marshal.PtrToStringAnsi(managedArray[i]);
            }
            return output;
        }

        /// <summary>
        /// Get names of available input loopback devices.
        /// </summary>
        /// <remarks>
        /// This will work only on Windows with the WASAPI API. Each input
        /// loopback device correspond to a real output device.
        /// If no device is available, the array will contain "noDevice".
        /// </remarks>
        /// <returns>array of device names</returns>
        public static string[] Exp_GetLoopbackDevicesNames()
        {
            string[] output = new string[0];
            int nbOutput = 0;
            IntPtr arrPtr = IntPtr.Zero;
            int ret = Ski_exp_getLoopbackDevicesNames(ref arrPtr, ref nbOutput);
            if (ret < 0)
                return output;
            if (arrPtr == IntPtr.Zero)
            {
                return output;
            }
            IntPtr[] managedArray = new IntPtr[nbOutput];
            Marshal.Copy(arrPtr, managedArray, 0, nbOutput);
            output = new string[nbOutput];
            for (int i = 0; i < nbOutput; i++)
            {
                output[i] = Marshal.PtrToStringAnsi(managedArray[i]);
            }
            return output;
        }

        /// <summary>
        /// Get available APIs for a given output device identified by name.
        /// </summary>
        /// <remarks>
        /// If no API is available, the array will contain "noAPI".
        /// </remarks>
        /// <param name="outputName">name of the output</param>
        /// <returns>array of api names</returns>
        public static string[] Exp_GetOutputDeviceAPIs(string outputName)
        {
            string[] apis = { "any_API" };
            int nbApis = 0;
            IntPtr arrPtr = IntPtr.Zero;
            int ret = Ski_exp_getOutputDeviceAPIs(outputName, ref arrPtr, ref nbApis);
            if (ret < 0)
                return apis;
            if (arrPtr == IntPtr.Zero)
            {
                return apis;
            }
            if (nbApis == 0)
            {
                apis[0] = "no_API";
                return apis;
            }
            IntPtr[] managedArray = new IntPtr[nbApis];
            Marshal.Copy(arrPtr, managedArray, 0, nbApis);
            apis = new string[nbApis + 1];
            apis[0] = "any_API";
            for (int i = 0; i < nbApis; i++)
            {
                apis[i + 1] = Marshal.PtrToStringAnsi(managedArray[i]);
            }
            return apis;
        }

        /// <summary>
        /// Get settings extremum values of the output device identified by
        /// name and API.
        /// </summary>
        /// <param name="outputName">name of the output</param>
        /// <param name="apiName">name of the API</param>
        /// <param name="maxChannels">max number of channel</param>
        /// <param name="defaultLowLatency">minimum latency of the output device</param>
        /// <param name="defaultHighLatency">maximum latency of the output device</param>
        /// <returns>0 if the values have been set successfully, an Error otherwise</returns>
        public static int Exp_GetOutputDeviceInfo(string outputName, string apiName, ref int maxChannels, ref float defaultLowLatency, ref float defaultHighLatency)
        {
            return Ski_exp_getOutputDeviceInfo(outputName, apiName, ref maxChannels, ref defaultLowLatency, ref defaultHighLatency);
        }

        /// <summary>
        /// Get all supported standard sample rates of the output device
        /// identified by name and API.
        /// </summary>
        /// <remarks>
        /// If the outputName or the API are not valid, the
        /// array is filled with all standard sample rates.
        /// </remarks>
        /// <param name="outputName">name of the output</param>
        /// <param name="apiName">name of the API</param>
        /// <returns>array of available frame rates</returns>
        public static int[] Exp_GetSupportedStandardSampleRates(string outputName, string apiName)
        {
            int[] framerates = new int[0];
            int nbFramerates = 0;
            IntPtr arrPtr = IntPtr.Zero;
            int ret = Ski_exp_getSupportedStandardSampleRates(outputName, apiName, ref arrPtr, ref nbFramerates);
            if (arrPtr == IntPtr.Zero)
            {
                return framerates;
            }
            framerates = new int[nbFramerates];
            Marshal.Copy(arrPtr, framerates, 0, nbFramerates);
            return framerates;
        }
        ///@}
    }
}

