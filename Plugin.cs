﻿using IPA;
using IPA.Config;
using IPA.Config.Stores;
using Skinetic_BeatSaber_Mod.Configuration;
using UnityEngine;
using IPALogger = IPA.Logging.Logger;

namespace Skinetic_BeatSaber_Mod
{
    [Plugin(RuntimeOptions.SingleStartInit)]
    public class Plugin
    {
        internal static Plugin Instance { get; private set; }
        internal static IPALogger Log { get; private set; }

        [Init]
        /// <summary>
        /// Called when the plugin is first loaded by IPA (either when the game starts or when the plugin is enabled if it starts disabled).
        /// [Init] methods that use a Constructor or called before regular methods like InitWithConfig.
        /// Only use [Init] with one Constructor.
        /// </summary>
        public void Init(IPALogger logger, Config conf)
        {
            Instance = this;
            Log = logger;
            Log.Info("Skinetic_BeatSaber_Mod initialized.");

            PluginConfig.Instance = conf.Generated<PluginConfig>();
        }

        [OnStart]
        public void OnApplicationStart()
        {
            //Log.Debug("OnApplicationStart");
            new GameObject("Skinetic_BeatSaber_ModController").AddComponent<Skinetic_BeatSaber_ModController>();

        }

        [OnExit]
        public void OnApplicationQuit()
        {
            //Log.Debug("OnApplicationQuit");

        }
    }
}
