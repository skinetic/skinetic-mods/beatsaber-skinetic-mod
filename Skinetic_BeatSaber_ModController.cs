﻿/*
 * This file is part of Skinetic BeatSaber Mod.
 *
 * Skinetic BeatSaber Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Skinetic Mod Template is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Skinetic BeatSaber Mod.  If not, see <http://www.gnu.org/licenses/>.
 */

using HarmonyLib;
using HMUI;
using Libraries.HM.HMLib.VR;
using Skinetic;
using Skinetic_BeatSaber_Mod.Configuration;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR;

namespace Skinetic_BeatSaber_Mod
{
    /// <summary>
    /// Monobehaviours (scripts) are added to GameObjects.
    /// For a full list of Messages a Monobehaviour can receive from the game, see https://docs.unity3d.com/ScriptReference/MonoBehaviour.html.
    /// </summary>
    public class Skinetic_BeatSaber_ModController : MonoBehaviour
    {
        public static Skinetic_BeatSaber_ModController Instance { get; private set; }
        private static SkineticManager SkineticHandler;
        private static int wooshID = -1;
        private static int multiplier = 1;
        private static string lastLeftHapticEffect;
        private static string lastRightHapticEffect;
        private static int arcLeftEffectID = -42;
        private static int arcRightEffectID = -42;
        private static DateTime leftLastUpdate = DateTime.Now;
        private static DateTime rightLastUpdate = DateTime.Now;
        private static System.Random random = new System.Random();

        // These methods are automatically called by Unity, you should remove any you aren't using.
        #region Monobehaviour Messages
        /// <summary>
        /// Only ever called once, mainly used to initialize variables.
        /// </summary>
        private void Awake()
        {
            // For this particular MonoBehaviour, we only want one instance to exist at any time, so store a reference to it in a static property
            //   and destroy any that are created while one already exists.
            if (Instance != null)
            {
                Plugin.Log?.Warn($"Instance of {GetType().Name} already exists, destroying.");
                GameObject.DestroyImmediate(this);
                return;
            }
            Plugin.Log?.Warn($"SKINETIC LOADING");
            GameObject.DontDestroyOnLoad(this); // Don't destroy this object on scene changes
            Instance = this;

            var harmony = new Harmony("com.actronika.skinetic.beatsabermod");
            // Always patched methods
            Plugin.Log?.Debug("Applying always-patched methods...");
            harmony.PatchAll(typeof(Skinetic_BeatSaber_ModController).Assembly);

            // Conditionally patch methods in the "env" region
            if (PluginConfig.Instance.MusicEffectsEnabled)
            {
                Plugin.Log?.Debug("Music effects enabled. Applying environment-related patches...");
                harmony.Patch(
                    AccessTools.Method(typeof(TrackLaneRingsRotationEffect), "AddRingRotationEffect"),
                    postfix: new HarmonyMethod(typeof(TrackLaneRingsRotationEffectPatch), nameof(TrackLaneRingsRotationEffectPatch.Postfix))
                );

                harmony.Patch(
                    AccessTools.Method(typeof(LightSwitchEventEffect), "HandleColorChangeBeatmapEvent"),
                    postfix: new HarmonyMethod(typeof(LightChangeEffectPatch), nameof(LightChangeEffectPatch.Postfix))
                );

                harmony.Patch(
                    AccessTools.Method(typeof(LightSwitchEventEffect), "HandleColorBoostBeatmapEvent"),
                    postfix: new HarmonyMethod(typeof(ColorBoostEffectPatch), nameof(ColorBoostEffectPatch.Postfix))
                );
            }
            else
            {
                Plugin.Log?.Debug("Music effects disabled. Skipping environment-related patches...");
            }

            SkineticHandler = new SkineticManager();
            SkineticHandler.Play(Patterns.Connect, SkineticManager.InitEffectProperties(1));

            SkineticHandler.SetFallbackPattern(Patterns.Kick, Patterns.None, 0.1f, 0);
            SkineticHandler.SetFallbackPattern(Patterns.Spin, Patterns.None, 0.5f, 2);

        }

        void Update()
        {
            if((DateTime.Now - leftLastUpdate).TotalSeconds >= 0.2 && arcLeftEffectID != -42)
            {
                SkineticHandler.Stop(arcLeftEffectID, 0.25f);
                arcLeftEffectID = -42;
                Plugin.Log?.Debug($"Stopping Arc Effect on node {XRNode.LeftHand} because of timeout");
            }

            if ((DateTime.Now - rightLastUpdate).TotalSeconds >= 0.2 && arcRightEffectID != -42)
            {
                SkineticHandler.Stop(arcRightEffectID, 0.25f);
                arcRightEffectID = -42;
                Plugin.Log?.Debug($"Stopping Arc Effect on node {XRNode.RightHand} because of timeout");
            }
        }

        /// <summary>
        /// Called when the script is being destroyed.
        /// </summary>
        private void OnDestroy()
        {
            if (SkineticHandler != null)
                SkineticHandler.Disconnect();

            //Plugin.Log?.Debug($"{name}: OnDestroy()");
            if (Instance == this)
                Instance = null; // This MonoBehaviour is being destroyed, so set the static instance property to null.
        }
        #endregion



        #region Skinetic Effects
        #region GamePlay

        [HarmonyPatch(typeof(ScoreController))]
        public static class ScoreControllerPatch
        {
            // Targeting the 'Start' method of ScoreController
            [HarmonyPostfix]
            [HarmonyPatch("Start")]
            public static void Start_Postfix(ScoreController __instance)
            {
                // Subscribe to the multiplierDidChangeEvent
                __instance.multiplierDidChangeEvent += OnMultiplierDidChange;
            }
        }

        private static void OnMultiplierDidChange(int newMultiplier, float normalizedProgress)
        {
            if (multiplier < newMultiplier)
            {
                SkineticHandler.Play(Patterns.X, SkineticManager.InitEffectProperties(8, 30 + multiplier * 7f, 0.92f + (multiplier / 50f)));
                //Plugin.Log?.Debug($"Playing Effect X");
            }
            multiplier = newMultiplier;
            //Plugin.Log?.Debug($"Multiplier changed {multiplier}");


        }


        [HarmonyPatch(typeof(TutorialNoteCutEffectSpawner), "HandleNoteWasCut")]
        [HarmonyPatch(typeof(NoteCutCoreEffectsSpawner), "HandleNoteWasCut")]
        class Slicing
        {

            static void Prefix(NoteCutInfo noteCutInfo)
            {
                SkineticSDK.EffectProperties effectProperties = SkineticManager.InitEffectProperties(1);

                //Plugin.Log?.Debug($"SaberType {noteCutInfo.saberType}");
                if (noteCutInfo.saberType == SaberType.SaberB)
                {
                    effectProperties.rightLeftInversion = true;
                }

                if (noteCutInfo.noteData.colorType == ColorType.None)
                {
                    //Plugin.Log.Warn("SLICED BOMB! hand: " + noteCutInfo.saberType);
                    effectProperties.priority = 5;
                    SkineticHandler.Play(Patterns.Bomb, effectProperties);
                    return;
                }

                effectProperties.speed = 1 + (multiplier / 30f);
                //Plugin.Log?.Debug($"playing with multiplier {multiplier}");
                if (!noteCutInfo.allIsOK) // When the cut is not perfect, the pattern is slowed down 
                {
                    SkineticHandler.Play(Patterns.ImperfectNote, effectProperties);
                }
                else
                {
                    SkineticHandler.Play(Patterns.PerfectNote, effectProperties);
                }
            }
        }

        [HarmonyPatch(typeof(HapticFeedbackManager), "PlayHapticFeedback")]
        class Haptics
        {

            static void Prefix(HapticFeedbackManager __instance, XRNode node, HapticPresetSO hapticPreset)
            {
                //Plugin.Log?.Debug($"Haptic feedback playing for {__instance.name} with preset {hapticPreset.name} on node {node}");
                if(node == XRNode.LeftHand)
                {
                    if (hapticPreset.name == "ArcSaberHapticPreset")
                    {
                        leftLastUpdate = DateTime.Now;
                    }
                    if (hapticPreset.name == "ArcSaberHapticPreset" && lastLeftHapticEffect != hapticPreset.name)
                    {
                        Plugin.Log?.Debug($"Starting Arc Effect on node {node}");
                        arcLeftEffectID = SkineticHandler.Play(Patterns.Arc, SkineticManager.InitEffectProperties(10, 100, 1, 0, 0, 0, 0, 0, false, 0, 0,0,false,false,true));
                    }
                    else if (lastLeftHapticEffect == "ArcSaberHapticPreset" && lastLeftHapticEffect != hapticPreset.name)
                    {
                        SkineticHandler.Stop(arcLeftEffectID, 0.01f);
                        Plugin.Log?.Debug($"Stopping Arc Effect on node {node}");
                        arcLeftEffectID = -42;
                    }
                    lastLeftHapticEffect = hapticPreset.name;
                }else if (node == XRNode.RightHand)
                {
                    if (hapticPreset.name == "ArcSaberHapticPreset")
                    {
                        rightLastUpdate = DateTime.Now;
                    }
                    if (hapticPreset.name == "ArcSaberHapticPreset" && lastRightHapticEffect != hapticPreset.name)
                    {
                        Plugin.Log?.Debug($"Starting Arc Effect on node {node}");
                        arcRightEffectID = SkineticHandler.Play(Patterns.Arc, SkineticManager.InitEffectProperties(10, 100));
                    }
                    else if (lastRightHapticEffect == "ArcSaberHapticPreset" && lastRightHapticEffect != hapticPreset.name)
                    {
                        SkineticHandler.Stop(arcRightEffectID, 0.01f);
                        arcRightEffectID = -42;
                        Plugin.Log?.Debug($"Stopping Arc Effect on node {node}");
                    }
                    lastRightHapticEffect = hapticPreset.name;
                }
 
            }

        }
                [HarmonyPatch(typeof(MissedNoteEffectSpawner), "HandleNoteWasMissed")]
        class Missed
        {
            static void Prefix(NoteController noteController)
            {
                if (noteController.noteData.colorType == ColorType.None) // Bomb are marked as non-colored note and should be missed
                    return;

                SkineticSDK.EffectProperties missedEffectProperties = SkineticManager.InitEffectProperties(2);
                missedEffectProperties.volume = 120;
                missedEffectProperties.overridePatternBoost = true;
                missedEffectProperties.effectBoost = 10;
                switch (noteController.noteData.lineIndex)
                {
                    case 0:
                        missedEffectProperties.heading = -45; break;
                    case 1:
                        missedEffectProperties.heading = -15; break;
                    case 2:
                        missedEffectProperties.heading = 15; break;
                    case 3:
                        missedEffectProperties.heading = 45; break;
                }
                missedEffectProperties.height = noteController.beatPos.y - 0.25f;
                SkineticHandler.Play(Patterns.Missed, missedEffectProperties);
                Plugin.Log.Warn($"Skinetic: Missed Note. beat Pos: {noteController.beatPos}, line Index: {noteController.noteData.lineIndex}");
            }
        }

        [HarmonyPatch(typeof(PlayerHeadAndObstacleInteraction), "RefreshIntersectingObstacles")]
        class ObstacleHit
        {
            static bool obstacleHit = false;
            static int instanceID = -1;
            static void Prefix(PlayerHeadAndObstacleInteraction __instance)
            {
                if (__instance.GetInstanceID() != instanceID)
                {
                    Plugin.Log?.Debug($"Registering headDidEnterObstacleEvent for instance {__instance.GetInstanceID()}");
                    __instance.headDidEnterObstacleEvent += OnHeadDidEnterObstacle;
                    instanceID = __instance.GetInstanceID();
                }
                if (__instance.playerHeadIsInObstacle && obstacleHit == false)
                {
                    obstacleHit = true;
                    //Plugin.Log.Warn("Skinetic: OBSTACLE - HEAD - START");
                }
                else if (!__instance.playerHeadIsInObstacle && obstacleHit == true)
                {
                    obstacleHit = false;
                    SkineticHandler.Stop(wooshID, 0.2f);
                    //Plugin.Log.Warn("Skinetic: OBSTACLE - HEAD - STOP");
                }
            }

            private static void OnHeadDidEnterObstacle(ObstacleController obstacleController)
            {
                obstacleHit = true;
                SkineticSDK.EffectProperties wallStayEffectProperties = SkineticManager.InitEffectProperties(6);
                if (obstacleController.obstacleData.lineLayer == NoteLineLayer.Base)
                {
                    switch (obstacleController.obstacleData.lineIndex)
                    {
                        case 0:
                            wallStayEffectProperties.heading = -45; break;
                        case 1:
                            wallStayEffectProperties.heading = -15; break;
                        case 2:
                            wallStayEffectProperties.heading = 15; break;
                        case 3:
                            wallStayEffectProperties.heading = 45; break;
                    }
                }
                else
                {
                    wallStayEffectProperties.tilting = 45;
                    wallStayEffectProperties.height = 0.6f;
                }
                wooshID = SkineticHandler.Play(Patterns.Wall | Patterns.Stay, wallStayEffectProperties);
                SkineticHandler.Play(Patterns.Wall | Patterns.Hit, wallStayEffectProperties);
                //Plugin.Log?.Debug($"Head entered in obstacle on line {obstacleController.obstacleData.lineIndex} with type {obstacleController.obstacleData.lineLayer}");

            }
        }

        [HarmonyPatch(typeof(PrepareLevelCompletionResults), "FillLevelCompletionResults")]
        class Result
        {
            static void Postfix(LevelCompletionResults.LevelEndStateType levelEndStateType)
            {
                SkineticHandler.Stop(wooshID, 0.2f); // Stop the wall effect in case it was hit just before the level end
                switch (levelEndStateType)
                {
                    case LevelCompletionResults.LevelEndStateType.Incomplete:
                        //Plugin.Log.Info("Level End State: Incomplete");
                        break;
                    case LevelCompletionResults.LevelEndStateType.Cleared:
                        SkineticHandler.Play(Patterns.LevelCleared, SkineticManager.InitEffectProperties(8));
                        //Plugin.Log.Info("Level End State: Cleared");
                        break;
                    case LevelCompletionResults.LevelEndStateType.Failed:
                        SkineticHandler.Play(Patterns.LevelFailed, SkineticManager.InitEffectProperties(8));
                        //Plugin.Log.Info("Level End State: Failed");
                        break;
                }
            }
        }

        #endregion
        #region UI
        [HarmonyPatch(typeof(EventSystemListener), "OnPointerEnter")]
        public static class EventSystemListenerEnter
        {
            static void Prefix(Selectable __instance, PointerEventData eventData)
            {
                SkineticHandler.Play(Patterns.UIHover, SkineticManager.InitEffectProperties(8));
                //Plugin.Log?.Debug($"Pointer entered: {__instance.name}");
            }
        }

        [HarmonyPatch(typeof(Selectable), "OnPointerEnter")]
        public static class SelectableEnter
        {
            static void Prefix(Selectable __instance, PointerEventData eventData)
            {
                SkineticHandler.Play(Patterns.UIHover, SkineticManager.InitEffectProperties(8));
                //Plugin.Log?.Debug($"Pointer entered: {__instance.name}");
            }
        }

        [HarmonyPatch(typeof(SelectableCell), "OnPointerEnter")]
        public static class SelectableCellEnter
        {
            static void Prefix(SelectableCell __instance, PointerEventData eventData)
            {
                SkineticHandler.Play(Patterns.UIHover, SkineticManager.InitEffectProperties(8));
                //Plugin.Log?.Debug($"Pointer entered: SelectableCell");
            }
        }

        [HarmonyPatch(typeof(Selectable), "OnPointerDown")]
        public static class SelectableClick
        {
            static void Prefix(Selectable __instance, PointerEventData eventData)
            {
                SkineticHandler.Play(Patterns.UIClick, SkineticManager.InitEffectProperties(8));
                //Plugin.Log?.Debug($"Pointer Clicked: {__instance.name}");
            }
        }

        [HarmonyPatch(typeof(SelectableCell), "OnPointerClick")]
        public static class SelectableCellClick
        {
            static void Prefix(SelectableCell __instance, PointerEventData eventData)
            {
                SkineticHandler.Play(Patterns.UIClick, SkineticManager.InitEffectProperties(8));
                //Plugin.Log?.Debug($"Pointer Clicked: SelectableCell");
            }
        }

        #endregion
        #region env

        [HarmonyPatch(typeof(TrackLaneRingsRotationEffect), "AddRingRotationEffect")]
        public class TrackLaneRingsRotationEffectPatch
        {

            public static void Postfix(TrackLaneRingsRotationEffectPatch __instance, float angle, float step, int propagationSpeed, float flexySpeed)
            {
                Plugin.Log?.Debug($"Spawned rotation effect with angle {angle}");
                SkineticSDK.EffectProperties effectPropertiesSpin = SkineticManager.InitEffectProperties(10);
                if (angle < 0)
                {
                    effectPropertiesSpin.rightLeftInversion = true;
                }
                effectPropertiesSpin.volume = (Math.Max((Math.Abs(angle) / 10f) + 64, 120)) * (PluginConfig.Instance.MusicEffectsVolume/100f);
                SkineticHandler.Play(Patterns.Spin, effectPropertiesSpin);
            }
        }

        [HarmonyPatch(typeof(LightSwitchEventEffect), "HandleColorChangeBeatmapEvent")]
        public class LightChangeEffectPatch
        {
            public static void Postfix(BasicBeatmapEventData basicBeatmapEventData)
            {
                int value = basicBeatmapEventData.value;
                Plugin.Log?.Debug($"Color change value {value}");
                Plugin.Log?.Debug($"Color change type {basicBeatmapEventData.basicBeatmapEventType}");
                Patterns pattern = Patterns.None;
                SkineticSDK.EffectProperties effectProperties = SkineticManager.InitEffectProperties(10);
                effectProperties.volume = PluginConfig.Instance.MusicEffectsVolume;
                switch (value)
                {
                    case 0: // laser type? not allways --> hats
                        {
                            Plugin.Log?.Debug($"Change of laser Type, event value {value}");
                            pattern = GetRandomHatPattern();
                            break;
                        }
                    case 1:
                    case 4:
                    case 5:
                    case 8:
                    case 9:
                    case 12: //Triggers sometimes, didn't found linked game element --> hats
                        {
                            Plugin.Log?.Debug($"Unknown color Change, event value {value}");
                            pattern = GetRandomHatPattern();
                            break;
                        }
                    case 2:
                    case 6:
                    case 10:// big changes --> kick
                        {
                            Plugin.Log?.Debug($"Big Color Change effect, event value {value}");
                            pattern = Patterns.Kick;
                            break;
                        }
                    case -1:
                    case 3:
                    case 7:
                    case 11: // pulse effect --> hats
                        {
                            Plugin.Log?.Debug($"Pulse Color effect, event value {value}");
                            pattern = GetRandomHatPattern();
                            break;
                        }
                }
                SkineticHandler.Play(pattern,effectProperties);
            }
            private static readonly System.Random _random = new System.Random();
            private static Patterns GetRandomHatPattern()
            {
                // Generate a random integer between 1 and 8 (inclusive)
                int randValue = _random.Next(1, 9); // 1 to 8

                // Compute the corresponding bit shift (1 << (randValue + 20))
                // Since H1 is 1 << 21, H2 is 1 << 22, ..., H8 is 1 << 28
                int shiftedValue = 1 << (randValue + 20);

                // Cast the integer to the Patterns enum
                Patterns selectedPattern = (Patterns)shiftedValue;

                return selectedPattern;
            }
        }

        [HarmonyPatch(typeof(LightSwitchEventEffect), "HandleColorBoostBeatmapEvent")]
        public class ColorBoostEffectPatch
        {
            public static void Postfix(ColorBoostBeatmapEventData eventData)
            {
                Plugin.Log?.Debug("Color boost effect");
                SkineticHandler.Play(Patterns.LightBoost, SkineticManager.InitEffectProperties(10, PluginConfig.Instance.MusicEffectsVolume));
            }
        }
        #endregion
        #endregion
    }
}
